﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGLooper : MonoBehaviour {

    //[SerializeField]
    public float speed = 0.6f;

    private Vector2 offset = Vector2.zero;
    private Material mat;

	// Use this for initialization
	void Start () {
        mat = GetComponent<Renderer>().material;
        offset = mat.GetTextureOffset("_MainTex");
	}
	
	// Update is called once per frame
	void Update () {
        float speedInput = speed * (1+ Input.acceleration.x); 
        offset.x += speedInput * Time.deltaTime;
        mat.SetTextureOffset("_MainTex", offset);

		
	}
}
