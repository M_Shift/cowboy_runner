﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

    [SerializeField]
    private GameObject[] obstacles;

    private List<GameObject> obstaclesForSpawining = new List<GameObject>();

    private void Awake()
    {
        InitialiseObstacles();
    }

    // Use this for initialization
    void Start () {
        StartCoroutine(SpawnRandomObstacle());
    }
	
	// Update is called once per frame
	void InitialiseObstacles () {

        int index = 0;

        for(int i = 0; i < obstacles.Length * 3; i++)
        {
            GameObject obj = Instantiate(obstacles[index], 
                new Vector3(transform.position.x, transform.position.y, -2), Quaternion.identity) as GameObject;
            obstaclesForSpawining.Add(obj);
            obstaclesForSpawining[i].SetActive(false);

            index++;

            if(index == obstacles.Length)
            {
                index = 0;
            }
        }
		
	}

    void shuffle()
    {
        for(int i = 0; i < obstaclesForSpawining.Count; i++)
        {
            GameObject temp = obstaclesForSpawining[i];
            int rand = Random.Range(i, obstaclesForSpawining.Count);
            obstaclesForSpawining[i] = obstaclesForSpawining[rand];
            obstaclesForSpawining[rand] = temp;
        }
    }

    IEnumerator SpawnRandomObstacle()
    {
        yield return new WaitForSeconds(Random.Range(1.5f, 4.5f));

        int index = Random.Range(0, obstaclesForSpawining.Count);

        while (true)
        {
            if (!obstaclesForSpawining[index].activeInHierarchy)
            {
                obstaclesForSpawining[index].SetActive(true);
                obstaclesForSpawining[index].transform.position = new Vector3(transform.position.x, transform.position.y, -2);
                break;
            }
            else
            {
                index = Random.Range(0, obstaclesForSpawining.Count);
            }
        }

        StartCoroutine(SpawnRandomObstacle());
    }
}
