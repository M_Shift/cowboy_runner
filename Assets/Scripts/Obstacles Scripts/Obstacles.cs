﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour {

    private float speed = -3f;

    private Rigidbody2D myBody;

	// Use this for initialization
	void Awake () {
        myBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        speed = -3f * (1 + Input.acceleration.x);
        myBody.velocity = new Vector2(speed, 0f);
	}
}
