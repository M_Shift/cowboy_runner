﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationScript : MonoBehaviour {

    private Animator anim;

	// Use this for initialization
	void Awake () {
        anim = GetComponent<Animator>();
	}

    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Obstacle")
        {
            anim.Play("Idle");
        }
    }

    private void OnCollisionExit2D(Collision2D target)
    {
        if (target.gameObject.tag == "Obstacle")
        {
            anim.Play("Run");
        }
    }
}
