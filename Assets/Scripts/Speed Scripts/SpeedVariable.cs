﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedVariable : MonoBehaviour {

    public static SpeedVariable instance;

    // Use this for initialization
    private void Awake()
    {
        instance = this;
    }

    private static float getTilt()
    {
        return Input.acceleration.x;
    }
}
